var should = require('should');
var itemModel = require('../models/Item');

describe('Item Model', () => {
 let model = {
  name: "Testing Item 001",
  itemCode: 'TI01',
  height: 20,
  width: 40,
  weight: 60,
  category: 'TESTITEM',
 }
 let item = new itemModel(model);
 it('Properties - Name', done => {
  item.name.should.equal(model.name);
  done();
 })
 it('Properties - Item Code', done => {
  item.itemCode.should.equal(model.itemCode);
  done();
 })
 it('Properties - Height', done => {
  item.height.should.equal(model.height);
  done();
 })
 it('Properties - Width', done => {
  item.width.should.equal(model.width);
  done();
 })
 it('Properties - Weight', done => {
  item.weight.should.equal(model.weight);
  done();
 })
 it('Properties - Category', done => {
  item.category.should.equal(model.category);
  done();
 })
})