var should = require('should');
var storageModel = require('../models/Storage');
var mongoose = require('mongoose');

describe('Storage Model', () => {
 let model = {
  location: mongoose.Types.ObjectId(),
  stock: mongoose.Types.ObjectId(),
  count: 20,
  remark: "This is a storage record",
 }
 let storage = new storageModel(model);
 it('Properties - Location', done => {
  storage.location.should.equal(model.location);
  done();
 });
 it('Properties - Stock', done => {
  storage.stock.should.equal(model.stock);
  done();
 });
 it('Properties - Count', done => {
  storage.count.should.equal(model.count);
  done();
 });
 it('Properties - Remark', done => {
  storage.remark.should.equal(model.remark);
  done();
 });
});