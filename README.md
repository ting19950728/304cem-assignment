# 304CEM Assignment

304CEM Assignment - Warehouse full stack web application

Instruction

**Step 1.**
- clone the repository

**Step 2.**
- $ npm install

**Step 3.**
- $ node index

**Step 4.**
- browse the system through browser on localhost:3000

**System Structure**
- index.js Core process file
- /routes Routing Table files
- /www    Web resources
- /models Data Models
- /controllers Request handlers
- /test   Auto Testing Files
- /bin    Other Resources

**Test**
- $ npm i -g mocha
- mocha

**Remarks**
- To Insert, Update, Delete, you must login first 