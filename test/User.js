var should = require('should');
var moment = require('moment');
var userModel = require('../models/User');

describe('User Model', () => {
 let model = {
  username: 'helloWorld',
  password: '123',
  firstName: 'Louis',
  lastName: 'Wong',
  dateOfBirth: '1995-07-28'
 }
 let user = new userModel(model);
 it('Properties - Username', done => {
  user.username.should.equal(model.username);
  done();
 });
 it('Properties - Password', done => {
  user.password.should.equal(model.password);
  done();
 });
 it('Properties - First Name', done => {
  user.firstName.should.equal(model.firstName);
  done();
 });
 it('Properties - Last Name', done => {
  user.lastName.should.equal(model.lastName);
  done();
 });
 it('Properties - Date of Birth', done => {
  let dob = moment(user.dateOfBirth).format('YYYY-MM-DD');
  dob.should.equal(model.dateOfBirth);
  done();
 });
 it('Properties - Full Name', done => {
  user.fullName.should.equal(`${model.firstName} ${model.lastName}`);
  done();
 });
})