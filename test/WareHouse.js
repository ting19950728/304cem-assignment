const should = require('should');
const mocha = require('mocha');
var wareHouseModel = require('../models/WareHouse');

describe('WareHouse Model', () => {
 let dt = Date.now();
 let model = {
  code: 'TEST999',
  name: 'WareHouse Test',
  address: '1/f, London Bridge',
  size: 'S',
  Storages: [],
  ModifiedDate: dt
 }
 let wareHouse = new wareHouseModel(model);
 it('Properties - Code', done => {
  wareHouse.code.should.equal(model.code);
  done();
 });
 it('Properties - Name', done => {
  wareHouse.name.should.equal(model.name);
  done();
 });
 it('Properties - Address', done => {
  wareHouse.address.should.equal(model.address);
  done();
 });
 it('Properties - size', done => {
  wareHouse.size.should.equal(model.size);
  done();
 });
 it(`Properties - Scale`, done => {
  wareHouse.Scale.should.equal(wareHouseModel.cprops.size[model.size]);
  done();
 });
 it('Properties - FullAddress', done =>{
  wareHouse.FullAddress.should.equal(`${model.name}, ${model.address}`);
  done();
 })
});